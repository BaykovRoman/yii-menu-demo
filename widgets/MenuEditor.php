<?php

namespace app\widgets;

use app\models\Menu;
use app\models\menu\Node;
use yii\base\Widget;
use yii\helpers\Html;

class MenuEditor extends Widget
{
    /** @var Menu */
    public $menu;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return "<div class='list-group list-group-root'>" . $this->renderNode($this->menu->rootNode) . "</div>";
    }

    /**
     * @param Node $node
     * @param int $depth
     * @return string
     */
    public function renderNode($node)
    {
        $result = "<div class='list-group-item'><div class='node-info'><div class='node-name'>" . Html::a($node->name,
                [$node->link]) . "</div><div>"
            . Html::a('Edit', ['/menu/edit-node', 'id' => $node->id], ['class' => 'btn btn-primary'])
            . Html::a('Add child', ['/menu/add-node', 'parentId' => $node->id], ['class' => 'btn btn-primary'])
            . (($node->left > 1) ? Html::a('Delete', ['/menu/remove-node', 'id' => $node->id],
                ['class' => 'btn btn-danger']) : '') . "</div></div>";
        if (!empty($node->children)) {
            $result .= "<div class='list-group'>";
            foreach ($node->children as $child) {
                $result .= $this->renderNode($child);
            }
            $result .= "</div>";
        }
        $result .= "</div>";

        return $result;
    }
}