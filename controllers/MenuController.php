<?php

namespace app\controllers;

use app\models\Menu;
use app\models\menu\Node;
use yii\web\Controller;

class MenuController extends Controller
{
    public function actionIndex()
    {
        $menu = (new Menu())->loadNodes();
        $node = new Node();
        return $this->render('index', [
            'node' => $node,
            'menu' => $menu,
        ]);
    }

    public function actionAddNode($parentId)
    {
        $node = new Node();

        if ($node->load(\Yii::$app->request->post()) && $node->validate()) {
            $menu = (new Menu)->loadNodes();
            $menu->insertNode($node, $menu->getNodeById($parentId));
            return $this->redirect('/menu');
        } else {
            $parentNode = (new Menu())->loadNodes()->getNodeById($parentId);
            return $this->render('add-node', ['node' => $node, 'parentNode' => $parentNode]);
        }
    }

    public function actionRemoveNode($id)
    {
        $menu = (new Menu)->loadNodes();
        $menu->removeNode($menu->getNodeById($id));
        return $this->redirect('/menu');
    }

    public function actionEditNode($id)
    {
        $node = Node::find()->where(['id' => $id])->one();
        if(\Yii::$app->request->isPost && $node->load(\Yii::$app->request->post()) && $node->validate()){
            $node->save();
            return $this->redirect('/menu');
        }else{
            return $this->render('edit-node', ['node' => $node]);
        }
    }
}