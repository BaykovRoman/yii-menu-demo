<?php
/** @var \app\models\menu\Node $parentNode */

/** @var \app\models\menu\Node $node */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$positionOptions[\app\models\menu\Node::POS_FIRST_CHILD] = 'First';
foreach ($parentNode->children as $child) {
    $positionOptions[$child->id] = "After " . $child->name;
}
$positionOptions[\app\models\menu\Node::POS_LAST_CHILD] = 'At the end';


$form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => ['class' => 'form-horizontal'],
]);
?>

<?= $form->field($node, 'name') ?>
<?= $form->field($node, 'link') ?>
<?= $form->field($node, 'position')->dropDownList($positionOptions); ?>

<div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end() ?>
