<?php
/** @var \app\models\menu\Node $node */

use yii\helpers\Html;
use yii\widgets\ActiveForm;


$form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => ['class' => 'form-horizontal'],
]);
?>

<?= $form->field($node, 'name') ?>
<?= $form->field($node, 'link') ?>

<div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end() ?>
