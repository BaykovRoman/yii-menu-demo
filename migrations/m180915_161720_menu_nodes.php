<?php

use yii\db\Migration;

/**
 * Class m180915_161720_menu_nodes
 */
class m180915_161720_menu_nodes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('menu_nodes', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'link' => $this->string(255),
            'left' => $this->integer()->notNull(),
            'right' => $this->integer()->notNull(),
        ]);

        $this->createIndex('menu-nodes-pk', 'menu_nodes', ['left', 'right']);

        $this->insert('menu_nodes',['name' => 'root-node', 'link' => '/', 'left' => 1, 'right' => 32]);
        $this->insert('menu_nodes',['name' => 'Product Information', 'link' => '/product-info', 'left' => 2, 'right' => 11]);
        $this->insert('menu_nodes',['name' => 'Overview', 'link' => '/product-info/overview', 'left' => 3, 'right' => 4]);
        $this->insert('menu_nodes',['name' => 'Features', 'link' => '/product-info/features', 'left' => 5, 'right' => 6]);
        $this->insert('menu_nodes',['name' => 'Screenshots', 'link' => '/product-info/screensots', 'left' => 7, 'right' => 8]);
        $this->insert('menu_nodes',['name' => 'Sampples', 'link' => '/product-info/samples', 'left' => 9, 'right' => 10]);
        $this->insert('menu_nodes',['name' => 'Downloads', 'link' => '/downloads', 'left' => 12, 'right' => 17]);
        $this->insert('menu_nodes',['name' => 'Mirror #1', 'link' => '/product-info/mirror1', 'left' => 13, 'right' => 14]);
        $this->insert('menu_nodes',['name' => 'Mirror #2', 'link' => '/downloads/mirror2', 'left' => 15, 'right' => 16]);
        $this->insert('menu_nodes',['name' => 'Purchase', 'link' => '/purchase', 'left' => 18, 'right' => 23]);
        $this->insert('menu_nodes',['name' => 'Byu online', 'link' => '/purchase/buy-online', 'left' => 19, 'right' => 20]);
        $this->insert('menu_nodes',['name' => 'Order help', 'link' => '/purchase/order-help', 'left' => 21, 'right' => 22]);
        $this->insert('menu_nodes',['name' => 'Support', 'link' => '/support', 'left' => 24, 'right' => 31]);
        $this->insert('menu_nodes',['name' => 'FAQ', 'link' => '/support/faq', 'left' => 25, 'right' => 26]);
        $this->insert('menu_nodes',['name' => 'Live support', 'link' => '/support/live-support', 'left' => 27, 'right' => 28]);
        $this->insert('menu_nodes',['name' => 'Forum', 'link' => '/support/forum', 'left' => 29, 'right' => 30]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('menu_nodes');
    }
}
