<?php

namespace app\models\menu;

use yii\base\Exception;
use yii\db\ActiveRecord;

/**
 * Class Node
 * @package app\models\menu
 * @property int $id
 * @property string $name
 * @property string $link
 * @property int $left
 * @property int $right
 */
class Node extends ActiveRecord
{

    const POS_FIRST_CHILD = 'first-child';
    const POS_LAST_CHILD = 'last-child';

    /** @var Node[] */
    public $children = [];
    /** @var Node */
    public $parent;
    /** @var string|int */
    public $position;

    public function attributeLabels()
    {
        return [
            'name' => 'Node name',
            'link' => 'Link',
        ];
    }

    public function rules()
    {
        return [
            [['name', 'link'], 'required'],
            [['name', 'link'], 'string', 'max' => 255],
            [['left', 'right'], 'integer'],
            ['position', 'default', 'value' => self::POS_FIRST_CHILD],
        ];
    }

    public static function tableName()
    {
        return '{{menu_nodes}}';
    }


    /**
     * @param Node[] $nodes
     * @return Node[]
     * @throws Exception
     */
    public function addChildren($nodes)
    {
        $nextLeft = $this->left + 1;

        while ($nextLeft < $this->right) {
            $nextChildKey = Node::getNodeKey($nodes, $nextLeft);
            $child = $nodes[$nextChildKey];
            unset($nodes[$nextChildKey]);
            $this->children[$child->id] = $child;
            $child->parent = $this;
            $nodes = $child->addChildren($nodes);
            $nextLeft = $child->right + 1;
        }
        return $nodes;
    }

    /**
     * @param Node[] $nodes
     * @param int $left
     * @return int
     * @throws Exception
     */
    static function getNodeKey($nodes, $left)
    {
        foreach ($nodes as $key => $node) {
            if ($node->left == $left) return $key;
        }
        throw new Exception("Node not found");
    }

    public function unlinkNode()
    {
        if (isset($this->parent->children[$this->id])) {
            unset($this->parent->children[$this->id]);
        }
        foreach ($this->children as $child) {
            $child->unlinkNode();
        }
    }
}