<?php

namespace app\models;

use app\models\menu\Node;
use yii\base\Exception;
use yii\base\Model;
use yii\db\Expression;

class Menu extends Model
{
    /** @var Node[] */
    protected $nodes;
    /** @var Node */
    public $rootNode;

    /**
     * @return $this
     * @throws Exception
     */
    public function loadNodes()
    {
        $this->nodes = Node::find()->orderBy('left')->all();
        $rootNodeKey = Node::getNodeKey($this->nodes, 1);
        $this->rootNode = $this->nodes[$rootNodeKey];
        $this->rootNode->addChildren($this->nodes);
        return $this;
    }

    /**
     * @param Node $targetNode
     */
    public function removeNode($targetNode)
    {
        $delta = $targetNode->right - $targetNode->left + 1;
        foreach ($this->nodes as $key => $node) {
            if ($node->left >= $targetNode->left && $node->right <= $targetNode->right) {
                $node->unlinkNode();
                unset($this->nodes[$key]);
            }
            if ($node->left > $targetNode->left) {
                $node->left -= $delta;
            }
            if ($node->right > $targetNode->right) {
                $node->right -= $delta;
            }
        }

        $deleteCondition = ['and',
            ['>=', 'left', $targetNode->left],
            ['<=', 'right', $targetNode->right],
        ];
        Node::deleteAll($deleteCondition);
        Node::updateAll([
            'left' => new Expression('`left` - ' . $delta),
        ], ['>', 'left', $targetNode->left]);
        Node::updateAll([
            'right' => new Expression('`right` - ' . $delta),
        ], ['>', 'right', $targetNode->right]);
    }

    /**
     * @param Node $newNode
     * @param Node $parentNode
     * @throws Exception
     */
    public function insertNode($newNode, $parentNode)
    {
        if (empty($parentNode->children)) {
            $newNode->left = $parentNode->left + 1;
            $newNode->right = $newNode->left + 1;
        } else {
            switch (true) {
                case $newNode->position == Node::POS_FIRST_CHILD:
                    reset($parentNode->children);
                    $currentFirst = current($parentNode->children);
                    $newNode->left = $currentFirst->left;
                    $newNode->right = $newNode->left + 1;
                    break;
                case $newNode->position == Node::POS_LAST_CHILD:
                    $currentLast = end($parentNode->children);
                    $newNode->left = $currentLast->right + 1;
                    $newNode->right = $newNode->left + 1;
                    break;
                default:
                    $insertAfter = $this->getNodeById($newNode->position);
                    $newNode->left = $insertAfter->right + 1;
                    $newNode->right = $newNode->left + 1;
                    break;
            }
        }

        foreach ($this->nodes as $node) {
            if ($node->left >= $newNode->left) $node->left += 2;
            if ($node->right >= $newNode->left) $node->right += 2;
        }

        Node::updateAll([
            'left' => new Expression('`left` + 2'),
        ], ['>=', 'left', $newNode->left]);
        Node::updateAll([
            'right' => new Expression('`right` + 2'),
        ], ['>=', 'right', $newNode->left]);
        $newNode->save();

        $this->nodes[] = $newNode;
        $parentNode->children[$newNode->id] = $newNode;
    }

    /**
     * @param int $id
     * @return Node
     * @throws Exception
     */
    public function getNodeById($id)
    {
        foreach ($this->nodes as $node) {
            if ($node->id == $id) return $node;
        }
        throw new Exception("Node not found");
    }
}